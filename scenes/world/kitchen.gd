extends Node2D

onready var food_spawner = $FoodSpawner
onready var monster = $Monster
onready var stomach = $Stomach
onready var victory = $Victory
onready var defeat = $Defeat

var Food = preload("res://scenes/game/food.tscn")
onready var food_frequency = $FoodFrequency

func start():
	food_frequency.start()
	stomach.value = 30

func _on_FoodFrequency_timeout():
	var food = Food.instance()
	food.position = food_spawner.position
	food.connect("cutted", self, "_on_food_cutted", [food])
	add_child(food)

func _on_Out_body_entered(body):
	body.queue_free()

func _on_food_cutted(polygon, cutted_food):
	var food = Food.instance()
	food.position = cutted_food.position
	add_child(food)
	food.defined_polygon(polygon)
	food.defined_texture(cutted_food.sprite_index)
	food.add_to_group("shard")
	cutted_food.queue_free()

func _on_Mouth_body_entered(body):
	if body.name == "Knife":
		return

	if !monster.is_mouth_open():
		monster.hurt()
		return
	
	if body.is_in_group("shard") == false:
		monster.hurt()
		return

	body.queue_free()
	monster.chewing()
	stomach.value += 6
	print(stomach.value)
	if stomach.value >= 100:
		victory.visible = true


func _on_hungry_timeout():
	stomach.value -= 1
	if stomach.value <= 0:
		defeat.visible = true
