extends KinematicBody2D

onready var collision_shape_2d = $CollisionShape2D
onready var knife_inclined = $"Knife-inclined"
onready var knife_to_cut = $"Knife-to-cut"
onready var audio_stream_player = $AudioStreamPlayer

var chops = [
	preload("res://assets/audio/Chop01.wav"),
	preload("res://assets/audio/Chop02.wav"),
	preload("res://assets/audio/Chop03.wav"),
	preload("res://assets/audio/Chop04.wav"),
	preload("res://assets/audio/Chop05.wav")
]

func _ready():
	randomize()

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		audio_stream_player.stream = chops[randi() % chops.size()]
		audio_stream_player.play()

	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.is_pressed():
		knife_inclined.visible = true
		knife_to_cut.visible = false
		collision_shape_2d.disabled = false
	if event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and !event.is_pressed():
		knife_inclined.visible = false
		knife_to_cut.visible = true
		collision_shape_2d.disabled = true

func _physics_process(delta):
	position = get_global_mouse_position()
