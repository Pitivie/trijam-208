extends Polygon2D

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()

func explode():
	var points = polygon

	var delaunay_points = Geometry.triangulate_delaunay_2d(points)
	
	print(delaunay_points)

	if not delaunay_points:
		print("error : no delaunay points found")

	for index in len(delaunay_points) / 3:
		var shard_pool = PoolVector2Array()
		for n in range (3):
			shard_pool.append(points[delaunay_points[(index * 3) + n]])
		
		var shard = Polygon2D.new()
		shard.polygon = shard_pool

		#shard.color = Color(randf(), randf(), randf(), 1)
		shard.texture = texture
		shard.position.x += index*50
		
		add_child(shard)
	
	color.a = 0

