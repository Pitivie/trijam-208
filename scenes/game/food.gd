extends RigidBody2D
onready var explode = $Explode
onready var collision_box = $CollisionBox

signal cutted

var shards = []
var food_sprites = [
	preload("res://assets/sprites/foods/hotdog.png"),
	preload("res://assets/sprites/foods/pizza.png"),
	preload("res://assets/sprites/foods/porkham.png"),
	preload("res://assets/sprites/foods/prokrib.png")
]
var sprite_index = 0

func _ready():
	randomize()
	sprite_index = randi() % food_sprites.size()
	explode.texture = food_sprites[sprite_index]
	collision_box.polygon = explode.polygon

func defined_texture(index):
	var sprite_index = index
	explode.texture = food_sprites[index]

func defined_polygon(value):
	collision_box.polygon = value.polygon
	explode.polygon = value.polygon

func _on_Food_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed():
		var points = explode.polygon

		var delaunay_points = Geometry.triangulate_delaunay_2d(points)

		if not delaunay_points:
			print("error : no delaunay points found")

		for index in len(delaunay_points) / 3:
			var shard_pool = PoolVector2Array()
			for n in range (3):
				shard_pool.append(points[delaunay_points[(index * 3) + n]])
			
			var shard = Polygon2D.new()
			shard.polygon = shard_pool

			shard.texture = explode.texture
			shard.position.x += index*50
			emit_signal("cutted", shard)
