extends Node2D

onready var animated_sprite = $AnimatedSprite
onready var close_mouth = $CloseMouth
onready var want_to_eat = $WantToEat
onready var audio_stream_player = $AudioStreamPlayer
onready var should_close_mouth = $ShouldCloseMouth

var chews_sound = [
	preload("res://assets/audio/Chew03.wav"),
	preload("res://assets/audio/Chew04.wav")
]
var angry_sound = preload("res://assets/audio/Angry01.wav")
var mount_sound = preload("res://assets/audio/Mouthopen01.wav")

var mouth_is_open = false
# Called when the node enters the scene tree for the first time.
func _ready():
	animated_sprite.play("default")

func _on_WantToEat_timeout():
	audio_stream_player.stream = mount_sound
	audio_stream_player.play()
	animated_sprite.play("open_mouth")
	mouth_is_open = true
	close_mouth.wait_time = 5
	close_mouth.start()
	want_to_eat.stop()

func _on_CloseMouth_timeout():
	animated_sprite.play("default")
	mouth_is_open = false
	want_to_eat.wait_time = randi() % 5 + 1 
	want_to_eat.start()

func hurt():
	audio_stream_player.stream = angry_sound
	audio_stream_player.play()
	animated_sprite.play("hurt")

func chewing():
	should_close_mouth.start()
	audio_stream_player.stream = chews_sound[randi() % chews_sound.size()]
	audio_stream_player.play()
	animated_sprite.play("chewing")
	close_mouth.wait_time = 2
	close_mouth.start()

func is_mouth_open():
	return mouth_is_open


func _on_ShouldCloseMouth_timeout():
	mouth_is_open = false
